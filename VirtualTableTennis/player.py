__author__ = 'Pavan Vemana'


class Player:
    def __init__(self, player):
        self.__player_name = player
        self.__score = 0
        self.__status = 'S'

    def get_player_name(self):
        return self.__player_name

    def hit_ball(self, game_ball):
        return game_ball.set_difficulty_level(int(input("%s How good are you on a scale of 1..4"
                                                        % self.get_player_name())))

    def update_score(self):
        self.__score += 1

    def get_score(self):
        return self.__score

    def get_status(self):
        return self.__status

    def set_status(self, status):
        self.__status = status