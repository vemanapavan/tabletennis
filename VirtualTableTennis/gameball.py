__author__ = 'Pavan Vemana'

from VirtualTableTennis.ttball import TTBall


class GameBall(TTBall):
    def __init__(self, color):
        super().__init__(color)
        self.difficulty_level = 0

    def set_difficulty_level(self, level):
        self.difficulty_level = level
        return self.difficulty_level

    def change_position(self):
        pass

    def get_color(self):
        return super().get_color()