__author__ = 'Pavan Vemana'

from VirtualTableTennis.player import Player
from VirtualTableTennis.gameball import GameBall
import random
from collections import deque


class Game:
    def __init__(self):
        """
        Initialize game daemon

        """
        self.__player1 = Player(input("Enter player 1's name"))
        self.__player2 = Player(input("Enter player 2's name"))
        self.__game_ball = GameBall(random.choice(['golden yellow', 'white']))
        self.__serves = deque([self.__player1, self.__player2])

    def begin(self):
        #self.__display()
        self.__player1.set_status('R')
        self.__player2.set_status('S')
        while not self.__is_game_over() and not self.__is_deuce():
            turn = self.__serves.pop()
            self.__move(turn.hit_ball(self.__game_ball), turn, False)
        else:
            if self.__is_deuce():
                self.__begin_deuce()
            self.__declare_winner()

    def __begin_deuce(self):
        print('Deuce')
        self.__display()
        self.__player1.set_status('R')
        self.__player2.set_status('S')
        while not self.__is_deuce_over():
            turn = self.__serves.pop()
            self.__move(turn.hit_ball(self.__game_ball), turn, True)

    def __is_deuce_over(self):
        p1_score = self.__player1.get_score()
        p1_name = self.__player1.get_player_name()
        p2_score = self.__player2.get_score()
        p2_name = self.__player2.get_player_name()

        print(u"{0:s}: {1:d} - {4:s}  <==> {2:s}: {3:d} - {5:s}".format(p1_name, p1_score, p2_name, p2_score,
                                                                        self.__player1.get_status(),
                                                                        self.__player2.get_status()))
        if abs(p1_score - p2_score) >= 2:
            self.result = self.__player1 if p1_score > p2_score else self.__player2
            return True
        else:
            return False

    def __move(self, player_move, player, deuce):
        if not self.__validate_move(player_move):
            if player is self.__serving_player():
                self.__receiving_player().update_score()
                self.__rotate(player, deuce)
            else:
                self.__serving_player().update_score()
                self.__rotate(player, deuce)
        else:
            self.__serves.appendleft(player)
            #print('Valid move', self.__serves)
        return

    def __rotate(self, player, deuce):
        score_sum = self.__player1.get_score() + self.__player2.get_score()
        if deuce:
            diff = abs(self.__player1.get_score() - self.__player2.get_score())
            if diff <= 1:
                self.__switch_players(player)
            else:
                if player is self.__serving_player():
                    self.__serves.append(player)
                else:
                    self.__serves.appendleft(player)
        else:
            if score_sum > 0 and score_sum % 2 == 0:
                self.__switch_players(player)
            else:
                if player is self.__serving_player():
                    self.__serves.append(player)
                else:
                    self.__serves.appendleft(player)

    def __switch_players(self, player):
        receiver = self.__receiving_player()
        server = self.__serving_player()
        receiver.set_status('S')
        server.set_status('R')
        if player is self.__serving_player():
            self.__serves.append(player)
        else:
            self.__serves.appendleft(player)
        #print('rotate if', self.__serves)

    def __serving_player(self):
        if self.__player1.get_status() == 'S':
            return self.__player1
        else:
            return self.__player2

    def __receiving_player(self):
        if self.__player1.get_status() == 'R':
            return self.__player1
        else:
            return self.__player2

    def __declare_winner(self):
        print("Game over")
        print("Winner : %s" % self.result.get_player_name())

    def __display(self):
        print("***************")
        print("* {0:4s} {1:4s}  *".format(self.__player1.get_player_name(), self.__player2.get_player_name()))
        print("* {0:3d} {1:3d}  *".format(self.__player1.get_score(), self.__player2.get_score()))
        print("***************")

    @staticmethod
    def __validate_move(difficulty_level):
        if 0 < difficulty_level < 5:
            return True
        else:
            return False

    def __is_game_over(self):
        p1_score = self.__player1.get_score()
        p2_score = self.__player2.get_score()

        self.__display()
        if p1_score >= 11 or p2_score >= 11 and p1_score - p2_score != 0:
            self.result = self.__player1 if p1_score > p2_score else self.__player2
            return True
        else:
            return False

    def __is_deuce(self):
        if self.__player1.get_score() == 10 and self.__player2.get_score() == 10:
            return True
        else:
            return False

if __name__ == "__main__":
    new_game = Game()
    new_game.begin()


